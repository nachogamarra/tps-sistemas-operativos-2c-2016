#include "RWLock.h"
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <chrono>
#include <thread>

using namespace std;

int globales = 0;
bool w = false;
RWLock lock;

// Con este test vemos que el read no es bloaqueante, y que el write si lo es.

void* random_read_write(void* puntero){
	int a = rand()%2;
	if (a == 0){
		lock.rlock();
		printf("Read %u entrando\n",(unsigned int) pthread_self());
		sleep(1);
		printf("Read %u saliendo\n",(unsigned int) pthread_self());
		lock.runlock();
	} else {
		lock.wlock();
		printf("Write %u entrando \n",(unsigned int) pthread_self());
		sleep(1);
		printf("Write %u saliendo \n",(unsigned int) pthread_self());
		lock.wunlock();
	}
	return NULL;
}

void* read_write(void* puntero){
	//int a = rand()%2;
	w = !w;
	if (w){
		lock.rlock();
		printf("Read %u entrando\n",(unsigned int) pthread_self());
		sleep(1);
		printf("Read %u saliendo\n",(unsigned int) pthread_self());
		lock.runlock();
	} else {
		lock.wlock();
		printf("Write %u entrando \n",(unsigned int) pthread_self());
		sleep(1);
		printf("Write %u saliendo \n",(unsigned int) pthread_self());
		lock.wunlock();
	}
	return NULL;
}



int main(int argc, const char* argv[]){
	int cantThreads = 100;
	pthread_t threads[cantThreads];
	// TEST 1
	printf("Test 1\n");
	for (int i = 0; i<cantThreads; i++){
		pthread_create(&threads[i],NULL,random_read_write,NULL);
	}
	for (int i = 0; i<cantThreads; i++){
		pthread_join(threads[i],NULL);
	}
	// TEST 2
	printf("Test 2\n");
	for (int i = 0; i<cantThreads; i++){
		pthread_create(&threads[i],NULL,read_write,NULL);
	}
	for (int i = 0; i<cantThreads; i++){
		pthread_join(threads[i],NULL);
	}

	return 0;
}
