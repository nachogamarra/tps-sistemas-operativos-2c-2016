#include "RWLock.h"

RWLock :: RWLock() {
  reading = 0;
//  writers = 0;
  writing = 0;
  pthread_mutex_init(&m,NULL);
  pthread_cond_init(&turn,NULL);
}

void RWLock :: rlock() {
  pthread_mutex_lock(&m);
  while(writing == 1){
    pthread_cond_wait(&turn, &m);
  }
  reading++;
  pthread_mutex_unlock(&m);
}

void RWLock :: wlock() {
  pthread_mutex_lock(&m);
   writers++;
  while(reading > 0 || writing == 1){
    pthread_cond_wait(&turn, &m);
  }
   writers--;
  writing++;
  pthread_mutex_unlock(&m);
}

void RWLock :: runlock() {
  pthread_mutex_lock(&m);
  reading--;
  if (reading == 0){
		pthread_cond_signal(&turn);
	}

  pthread_mutex_unlock(&m);
}

void RWLock :: wunlock() {
  pthread_mutex_lock(&m);
  writing--;

  pthread_cond_broadcast(&turn);

  pthread_mutex_unlock(&m);
}
