#ifndef __SCHED_MFQ__
#define __SCHED_MFQ__

#include <vector>
#include <queue>
#include "basesched.h"

using namespace std;
class SchedMFQ : public SchedBase {
	public:
		enum estado_proc {READY, RUNNING, NOT_RUNNING, DISABLED};
		class proc {
			public:
				proc(int pid) {
					this -> pid = pid;
					this -> estado = READY;
					this -> quantum = 0;
					this -> fue_bloqueada = false;
					this -> cola = 0;
				}
				int pid;
				int quantum;
				bool fue_bloqueada;
				estado_proc estado;
				unsigned int cola;
		};
		SchedMFQ(std::vector<int> argn);
        ~SchedMFQ();
		virtual void initialize() {};
		virtual void load(int pid);
		virtual void unblock(int pid);
		virtual int tick(int n, const enum Motivo m);
		int next();
		//void cambiar_estado_tarea(int pid, estado_proc estado);
		//proc get_proc(int pid);

	private:
		std::vector<std::queue<int> > vq;
    std::vector<int> def_quantum;
    std::vector<std::vector<int> > unblock_to;
    int quantum, n, prioridad_actual;
		//Usamos una estructura distinta a la que venía en el .h
};

#endif
