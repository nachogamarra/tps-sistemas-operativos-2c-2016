#ifndef __SCHED_SJF__
#define __SCHED_SJF__

#include <vector>
#include <queue>
#include <algorithm>
#include "basesched.h"

using namespace std;

class SchedSJF : public SchedBase {
	public:
		SchedSJF(std::vector<int> argn);
        ~SchedSJF();
		virtual void initialize() {};
		virtual void load(int pid);
		virtual void unblock(int pid);
		virtual int tick(int cpu, const enum Motivo m);	
		int prox_pid(int cpu);
	private:
		vector<int> tiempos;
		enum estado_proc {READY, RUNNING, NOT_RUNNING};
		class proc {
			public:
				proc(int pid, int tiempo) {
					this -> pid = pid;
					this -> tiempo = tiempo;
					estado = READY;
				}
				int pid;
				int tiempo;
				estado_proc estado;
		};
		vector<proc> procesos;
};

#endif
