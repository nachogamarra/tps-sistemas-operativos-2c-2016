#include <vector>
#include <queue>
#include "sched_rr.h"
#include "basesched.h"
#include <iostream>

using namespace std;
int procesos_en_cola;
int ultimo_pid = -1;
int procesos_size;

SchedRR::SchedRR(vector<int> argn) {
	// Round robin recibe la cantidad de cores y sus cpu_quantum por parámetro
	this -> quantums = vector<int> (argn);
	procesos_en_cola = 0;
	procesos_size = 0;
}

SchedRR::~SchedRR() {
/* completar */
}


void SchedRR::load(int pid) {
	SchedRR::proc p = SchedRR::proc(pid); //crea un nuevo proceso
	procesos.push_back(p);								//para agregar a la cola
	procesos_en_cola++;
	procesos_size++;
}

void SchedRR::unblock(int pid) {
	procesos[pid].estado = READY;
}

int SchedRR::tick(int cpu, const enum Motivo m) {
	if (m == TICK) {
		if (current_pid(cpu) != IDLE_TASK) { //Si esta corriendo IDLE_TASK directamente busca prox_pid
			if (procesos[current_pid(cpu)].quantum == 0) { //si no le quedan quantums busca prox_pid
				procesos[current_pid(cpu)].estado = READY;
			}
			else {
				procesos[current_pid(cpu)].quantum--; // de lo contrario decrementa los quantums usados
				return current_pid(cpu); //devuelve la misma tarea porque todavia tiene quantums disponibles
			}
		}
	}
	else {
		procesos[current_pid(cpu)].estado = NOT_RUNNING; //desactiva la tarea ya sea por bloqueo o porque termino
		if (m == EXIT) procesos_en_cola--; //si la tarea termino decrementa los procesos en cola
	}
	return prox_pid(cpu);
}

int SchedRR::prox_pid(int cpu) {
	if (procesos_en_cola > 0) {
		for (int i = ultimo_pid + 1, j = 0; j <= procesos_size; i++, j++) { //a partir del ultimo proceso corrido pasa al siguiente
			if (procesos[i % procesos_size].estado == READY) { //si esta disponible para correr entonces lo asigna a correr
				ultimo_pid = procesos[i % procesos_size].pid;
				procesos[i % procesos_size].quantum = quantums[cpu + 1];
				procesos[i % procesos_size].estado = RUNNING;
				return ultimo_pid;
			}
		}
	}
	return IDLE_TASK; //si no hay ninguno disponible o directamente no hay procesos en cola entonces devuelve IDLE_TASK
}
