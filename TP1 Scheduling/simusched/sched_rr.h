#ifndef __SCHED_RR__
#define __SCHED_RR__

#include <vector>
#include <queue>
#include <algorithm>
#include "basesched.h"

using namespace std;

class SchedRR : public SchedBase {
	public:
		SchedRR(std::vector<int> argn);
        ~SchedRR();
		virtual void initialize() {};
		virtual void load(int pid);
		virtual void unblock(int pid);
		virtual int tick(int cpu, const enum Motivo m);	
		int prox_pid(int cpu);
	private:
		vector<int> quantums;
enum estado_proc {READY, RUNNING, NOT_RUNNING};
		class proc {
			public:
				proc(int pid) {
					this -> pid = pid; 
					this -> estado = READY;
					this -> quantum = 0;
				}
				int pid;
				int quantum;
				estado_proc estado; 
		};
		vector<proc> procesos;
};

#endif
