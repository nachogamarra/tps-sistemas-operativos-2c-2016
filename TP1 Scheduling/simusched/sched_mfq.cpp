#include <vector>
#include <queue>
#include "sched_mfq.h"
#include "basesched.h"
#include <iostream>

using namespace std;
vector<int> ultimo_pid_cola;

SchedMFQ::SchedMFQ(vector<int> argn) {
	// MFQ recibe los quantums por parÃ¡metro.
	  quantum = 0;  //Contador de quantos para el proceso corriendo.
	  n = argn[1]; //Cantidad de colas.
	  prioridad_actual = 0;
	  for(int i = 2; i < n+2; i++){ //Itero las prioridades.
	    vq.push_back(queue<int>()); //Armo las colas para los procesos para cada prioridad.
			cerr << argn[i] << endl;

			def_quantum.push_back(argn[i]); //pusheo su respectiva cuota de quantum
	    unblock_to.push_back(vector<int>());  //Esto es un vector de vectores, lo uso para saber a que prioridad volver dsps de un block
	  }
}

SchedMFQ::~SchedMFQ() {
/* llenar */
}

void SchedMFQ::load(int pid) {
  vq[0].push(pid);
}

void SchedMFQ::unblock(int pid) {
	int prioridad = -1;
   for(int i = 0; i < n && prioridad == -1; i++){
     vector<int> vecActual = unblock_to[i];

     for(unsigned int j = 0; j < vecActual.size(); j++){
       if(vecActual[j] == pid){
         vecActual.erase(vecActual.begin() + j);
         prioridad = i;
         break;
       }//end if
     }//end for

   }//end for
   vq[prioridad].push(pid);  //Pusheo en la prioridad que tenía que volver.
}



int SchedMFQ::tick(int cpu, const enum Motivo m) {
	if(m == TICK && current_pid(cpu) != IDLE_TASK) quantum++; //Corrí un tick, aumento un quanto

 bool vacias = true;
 for(int i = 0; i <n; i++){
	 vacias &= vq[i].empty();
 }//Para saber si hay procesos además de yo.


 if(m == EXIT || m == BLOCK){

	 if(m == BLOCK){ //Si me bloqueo tengo que guardar una prioridad menor de la que estoy, cuando vuelva soy mas prioritario.
		 int bajar_pri = prioridad_actual == 0 ? 0 : prioridad_actual-1;
		 unblock_to[bajar_pri].push_back(current_pid(cpu));
	 }

	 if(vacias) return IDLE_TASK;  //Si no hay otra para correr corre la IDLE
	 return next();
 }

 else if(current_pid(cpu) == IDLE_TASK && !vacias){  //Soy la IDLE y hay gente para correr.
	 return next();
 }

 else if(quantum >= def_quantum[prioridad_actual]){	//Si terminé mi quanto voy a la cola de la prioridad siguiente (a menos prioritario)
	 prioridad_actual = prioridad_actual == n-1 ? n-1 : prioridad_actual +1;

	 if(vacias){ //Si no hay gente para correr restauro el quanto y vuelvo a correr otra cuota de quanto.
		 quantum = 0;
		 return current_pid(cpu);
	 }
		vq[prioridad_actual].push(current_pid(cpu));	//Me pusheo en la cola que sigue de menos prioridad
		return next(); //el que sigue
 }

 return current_pid(cpu); //Si no sucede nada de lo anterior sigo corriendo
}

int SchedMFQ::next(void) { //Restauro el quanto y agarro un proceso de la cola mÃ¡s prioritaria que no estÃ¡ vacÃ­a.
  // Elijo el nuevo pid
  quantum = 0;
  int i;
  for(i = 0; i < n && vq[i].empty(); i++ );  //i va a ser la mejor prioridad que tiene gente para correr
  prioridad_actual = i;

  int sig = vq[prioridad_actual].front();
  vq[prioridad_actual].pop();
  return sig;

}
