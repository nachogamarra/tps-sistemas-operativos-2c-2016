#include <vector>
#include <queue>
#include <iostream>
#include "sched_sjf.h"

using namespace std;

SchedSJF::SchedSJF(vector<int> argn) {
        // Recibe la cantidad de cores y los tiempos de ejecución de cada tarea en el lote
	tiempos = vector<int> (argn);
}

SchedSJF::~SchedSJF() {
/* llenar */
}

void SchedSJF::load(int pid) {
	SchedSJF::proc p = SchedSJF::proc(pid, tiempos[pid + 1]);
	procesos.push_back(p);

}

void SchedSJF::unblock(int pid) {
	procesos[pid].estado = READY;
}

int SchedSJF::tick(int cpu, const enum Motivo m) {
	if (m == TICK || m == BLOCK) {
		if (current_pid(cpu) != IDLE_TASK) {return current_pid(cpu);}
	} else {
		procesos[current_pid(cpu)].estado = NOT_RUNNING;
	}
	return prox_pid(cpu);
}

int SchedSJF::prox_pid(int cpu) {
	unsigned int i;
	int _prox_pid = IDLE_TASK, tiempo;

	if (procesos.size() == 1) {
		if (procesos[0].estado == READY) {
			_prox_pid = procesos[0].pid;
		}
	}
	for (i = 0; i < procesos.size(); i++) {
		if (procesos[i].estado == READY) {
			_prox_pid = procesos[i].pid;
			tiempo = procesos[i].tiempo;
			break;
		}
	}
	for (++i; i < procesos.size(); i++) {
		if ((procesos[i].estado == READY) && (procesos[i].tiempo < tiempo)) {
			_prox_pid = procesos[i].pid;
			tiempo = procesos[i].tiempo;
		}
	}
	if (_prox_pid != IDLE_TASK) {
		procesos[_prox_pid].estado = RUNNING;
	}
	return _prox_pid;
}
