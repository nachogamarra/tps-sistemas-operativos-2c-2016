#ifndef __SCHED_RSJF__
#define __SCHED_RSJF__

#include <vector>
#include <queue>
#include <algorithm>
#include "basesched.h"

using namespace std;

class SchedRSJF : public SchedBase {
	public:
		SchedRSJF(std::vector<int> argn);
        ~SchedRSJF();
		virtual void initialize() {};
		virtual void load(int pid);
		virtual void unblock(int pid);
		virtual int tick(int cpu, const enum Motivo m);	
		int prox_pid(int cpu);
	private:
		vector<int> tiempos;
		vector<int> cpu_quantum;
		enum estado_proc {READY, RUNNING, NOT_RUNNING};
		class proc {
			public:
				proc(int pid, int tiempo) {
					this -> pid = pid;
					this -> tiempo = tiempo;
					estado = READY;
				}
				int pid;
				int tiempo;
				int quantums;
				estado_proc estado;
		};
		vector<proc> procesos;
};

#endif
