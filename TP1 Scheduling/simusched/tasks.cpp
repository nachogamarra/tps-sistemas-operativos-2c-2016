#include "tasks.h"
#include <stdlib.h>
#include <stdio.h>
using namespace std;

void TaskCPU(int pid, vector<int> params) { // params: n
	uso_CPU(pid, params[0] - 1); // Uso el CPU n milisegundos.
}

void TaskIO(int pid, vector<int> params) { // params: ms_pid, ms_io,
	uso_CPU(pid, params[0]); // Uso el CPU ms_pid milisegundos.
	uso_IO(pid, params[1]); // Uso IO ms_io milisegundos.
}

void TaskAlterno(int pid, vector<int> params) { // params: ms_pid, ms_io, ms_pid, ...
	for(int i = 0; i < (int)params.size(); i++) {
		if (i % 2 == 0) uso_CPU(pid, params[i] - 1);
		else uso_IO(pid, params[i]);
	}
}

void TaskConsola(int pid, vector<int> params) { //params: n, bmin, bmax
	srand(time(NULL));
	for (int i = 0; i < params[0]; i++) {
		int tiempo = (rand() % (params[2] - params[1] + 1)) + params[1];
		uso_IO(pid, tiempo);
		uso_CPU(pid, 1);
	}
}

void TaskBatch(int pid, vector<int> params) { //params: total_cpu, cant_bloqueos
	srand(time(NULL));
	
	int total_cpu = params[0];
	int cant_bloqueos = params[1];
	int size_total = total_cpu-1;

	int* shufflear = new int[size_total];
	int* res = new int[size_total];

	int indice = 0;
	
	while(cant_bloqueos > 0){
		shufflear[indice] = 1;
		cant_bloqueos--;
		total_cpu--;
		indice++;
	}
	while(total_cpu > 0){
		shufflear[indice] = 0;
		total_cpu--;
		indice++;
	}

	int i_res = 0;
	int mezclar = size_total;
	while(mezclar > 0){

		int i_random = (rand() %(mezclar));

		while(shufflear[i_random] == -1){
			i_random++;
		}

		res[i_res] = shufflear[i_random];
		shufflear[i_random] = -1;
		i_res++;
		mezclar--;
	}
	
	for(int i = 0; i < size_total; i ++){
		if(res[i] == 1) uso_IO(pid,2);
		else if(res[i] == 0) uso_CPU(pid, 1);
	}
	free(res);
	free(shufflear);
}

void tasks_init(void) {
	/* Todos los tipos de tareas se deben registrar acá para poder ser usadas.
	 * El segundo parámetro indica la cantidad de parámetros que recibe la tarea
	 * como un vector de enteros, o -1 para una cantidad de parámetros variable. */
	register_task(TaskCPU, 1);
	register_task(TaskIO, 2);
	register_task(TaskAlterno, -1);
	register_task(TaskConsola, 3);
	register_task(TaskBatch,2);
}
