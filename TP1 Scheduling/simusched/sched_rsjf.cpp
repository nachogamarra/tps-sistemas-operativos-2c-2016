#include <vector>
#include <queue>
#include <iostream>
#include "sched_rsjf.h"

using namespace std;

SchedRSJF::SchedRSJF(vector<int> argn) {
	tiempos = vector<int> (argn);
	cpu_quantum = vector<int> (argn);
}

SchedRSJF::~SchedRSJF() {
}

void SchedRSJF::load(int pid) {
	SchedRSJF::proc p = SchedRSJF::proc(pid, tiempos[pid + tiempos[0] + 1]);
	procesos.push_back(p);
}

void SchedRSJF::unblock(int pid) {
	procesos[pid].estado = READY;
}

int SchedRSJF::tick(int cpu, const enum Motivo m) {
	if (m == TICK) {
		if (current_pid(cpu) != IDLE_TASK) {
			if (cpu_quantum[cpu + 1] == 0) { // si se acabo el quantum de este cpu entonces lo resetea y deja a la tarea READY
				cpu_quantum[cpu + 1] = tiempos[cpu + 1];
				if (procesos[current_pid(cpu)].tiempo == 0) { //termino la tarea
					procesos[current_pid(cpu)].estado = NOT_RUNNING;
				} else {
					procesos[current_pid(cpu)].estado = READY;
				}
				return prox_pid(cpu);
			} else {
				cpu_quantum[cpu + 1]--;
				if (procesos[current_pid(cpu)].tiempo == 0) { //termino la tarea
					procesos[current_pid(cpu)].estado = NOT_RUNNING;
					cpu_quantum[cpu + 1] = tiempos[cpu + 1];
					return prox_pid(cpu);
				} else {
					procesos[current_pid(cpu)].tiempo--; //si no termino la tarea entonces decrementa el tiempo consumido
					return current_pid(cpu); //y sigue ejecutando la misma tarea
				}
			}
		} else { //si estaba en IDLE
			if (cpu_quantum[cpu + 1] == 0) { // si es un nuevo lote de quantums entonces busca la proxima tarea (que puede ser la misma)
				cpu_quantum[cpu + 1] = tiempos[cpu + 1];
			}
			int pid = prox_pid(cpu);
			if (pid != IDLE_TASK) {
				cpu_quantum[cpu + 1] = tiempos[cpu + 1];
			}
			return pid;
		}
	} else { //block o exit
		procesos[current_pid(cpu)].estado = NOT_RUNNING;
		if (cpu_quantum[cpu + 1] == 0) {
			cpu_quantum[cpu + 1] = tiempos[cpu + 1];
		} else {
			cpu_quantum[cpu + 1]--;
		}
		return prox_pid(cpu);
	}
}

int SchedRSJF::prox_pid(int cpu) {
	unsigned int i;
	int _prox_pid = IDLE_TASK, tiempo;

	if (procesos.size() == 1) {
		if (procesos[0].estado == READY) {
			_prox_pid = procesos[0].pid;
		}
	} else {
		for (i = 0; i < procesos.size(); i++) {
			if (procesos[i].estado == READY) {
				_prox_pid = procesos[i].pid;
				tiempo = procesos[i].tiempo;
				break;
			}
		}
		for (++i; i < procesos.size(); i++) {
			if ((procesos[i].estado == READY) && (procesos[i].tiempo < tiempo)) {
				_prox_pid = procesos[i].pid;
				tiempo = procesos[i].tiempo;
			}
		}
		if (_prox_pid != IDLE_TASK) {
			procesos[_prox_pid].estado = RUNNING;
		}
	}
	return _prox_pid;
}
