El ejercicio 3 plantea agregar un nuevo tipo de tarea al archivo \texttt{tasks.cpp}. Esta tarea se llama \texttt{TaskBatch} y recibe dos
parámetros: \textit{total\_cpu} y \textit{cant\_bloqueos}. Esta tarea debe realizar \textit{cant\_bloqueos} llamadas bloqueantes que duren  ticks
de reloj en tiempos elegidos pseudoaleatoriamente y al mismo tiempo utilizar \textit{total\_cpu} ticks de reloj.
Esto significa que va a realizar \textit{cant\_bloqueos} llamadas bloqueantes y \textit{total\_cpu - cant\_bloqueos} ciclos que utilizan
el CPU durante un tick de reloj, de forma de utilizar el CPU durante \textit{total\_cpu} ticks de reloj sin contar el tiempo en que la tarea permanece
bloqueada y considerando que, realizar una llamada bloqueante utiliza un tick de reloj antes de bloquearse.

\begin{verbatim}
void TaskBatch(int pid, vector<int> params) { // params: total_cpu, cant_bloqueos
    int* llamadas = new int[params[0]];
    int random = 0;
    int i, aux;
    srand(time(NULL) * pid);
	
    	for (i = 0; i < params[1]; i++) {
        	llamadas[i] = 1;
    }
    for (i = params[1]; i < params[0]; i++) {
        llamadas[i] = 0;
    }
	
    	for (i = 0; i < params[0]; i++) {
        	random = rand() % params[0];
        aux = llamadas[i];
        llamadas[i] = llamadas[random];
        llamadas[random] = aux;
    }
	
    	for (i = 0; i < params[0]; i++) {
        if (llamadas[i] == 0) uso_CPU(pid, 1);
        else uso_IO(pid, 2);
    	}
}
\end{verbatim}

Para generar las llamadas bloqueantes necesarias en tiempos elegidos pseudoaleatoriamente, comenzamos por crear un vector de tama\~no \textit{total\_cpu} que tiene \textit{cant\_bloqueos} valores iguales a uno y el resto iguales a cero. 
Cada 1 representa una llamada bloqueante y cada 0 un uso intensivo del CPU. Una vez que tenemos el vector inicializado, el siguiente paso es desordenarlo
de forma pseudoaleatoria. Para lo cual, realizamos \textit{total\_cpu} intercambios, donde en cada uno el elemento en la posición $i$ se intercambia por
el elemento en una posición elegida pseudoaleatoriamente dentro del vector. Esta posición se elige utilizando la función \texttt{rand()} 
y se la restringe al tama\~no del vector usando el m\'odulo, de forma similar a como se hizo la tarea \texttt{TaskConsole}.

Finalmente se recorre el vector de unos y ceros distribuidos pseudoaleatoriamente y por cada cero se ejecuta un uso intensivo del CPU durante 1 tick de reloj y por cada uno, una llamada bloqueante que bloqueara a la tarea durante 2 ticks de reloj.

A continuacion se muestra el lote de las 4 tareas Batch y su grafico usando un scheduler FCFS:

\begin{verbatim}
TaskBatch 4 4
Taskbatch 8 4
TaskBatch 9 3
TaskBatch 1 1
\end{verbatim}

